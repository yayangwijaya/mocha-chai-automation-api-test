const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

describe('Delete user', () => {
  it('should delete a user which ID = 2', (done) => {
    chai.request('https://reqres.in')
      .delete('/api/users/2')
      .end((err, res) => {
        expect(res).to.have.status(204);
        done();
      });
  });
});
