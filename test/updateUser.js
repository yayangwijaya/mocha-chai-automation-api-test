const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

describe('API Tests', () => {
  it('Success update user which ID 2', (done) => {
    const payload = {
      name: 'Yayang',
      job: 'Wijaya'
    };

    chai.request('https://reqres.in')
      .put('/api/users/2')
      .send(payload)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name', 'Yayang');
        expect(res.body).to.have.property('job', 'Wijaya');
        done();
      });
  });
});
