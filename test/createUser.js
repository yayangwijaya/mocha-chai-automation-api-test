const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

describe('API Tests', () => {
  it('Success create a new user', (done) => {
    const payload = {
      name: 'Yayang',
      job: 'Wijaya'
    };

    chai.request('https://reqres.in')
      .post('/api/users')
      .send(payload)
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.body).to.have.property('name', 'Yayang');
        expect(res.body).to.have.property('job', 'Wijaya');
        done();
      });
  });

  it('Leave mandatory blank field, Should show error message', (done) => {
    const payload = {
      name: '',
      job: ''
    };

    chai.request('https://reqres.in')
      .post('/api/users')
      .send(payload)
      .end((err, res) => {
        expect(res).to.have.status(201);
        expect(res.body).to.have.property('name', '');
        expect(res.body).to.have.property('job', '');
        done();
      });
  });

});
